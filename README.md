This is Java WEB application, that is written on: Java EE 7, JSF 2, Primefaces, Hibernate, PostgreSQL.

Application provides following functionality:  

1. User creation;  
2. User authentication and authorization.  
3. Calculator.  
4. Calculator actions logging, logs storage in DB and observation possibility.

How to run:  

- Clone project from https://bitbucket.org/AnnaPolakova/calculator/   
- Open project in IDE and in hibernate.properties specify correct parameters of your PostgreSQL DB.  
- Execute following scripts in your PostgreSQL DB:  

CREATE TABLE USERS(  
	ID serial PRIMARY KEY NOT NULL,  
	USER_NAME VARCHAR(50) NOT NULL,  
	PASSWORD VARCHAR(100) NOT NULL,  
	LAST_ACCESS DATE  
);

CREATE TABLE LOGS(  
	ID serial PRIMARY KEY NOT NULL,  
	NAME VARCHAR(100) NOT NULL,  
	LOGS TEXT,  
	DATE_CREATED DATE,  
	USER_ID int4 REFERENCES USERS(ID) NOT NULL  
);

- Run application via maven tomcat7 configuration - mvn tomcat7:run-war, using the development profile, specified in pom.xml.