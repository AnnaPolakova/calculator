package com.me.calculator.dao;

import com.me.calculator.model.Log;
import com.me.calculator.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public class LogDAO {

    private EntityManager em = HibernateUtils.getEm();

    public List<Log> getAllLogs() {
        try {
            List<Log> logList = (List<Log>) em
                    .createQuery("SELECT l from logs l order by l.id desc").getResultList();

            return logList;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Log> getLogsByUserId(Long userId) {
        try {
            List<Log> logList = (List<Log>) em
                    .createQuery("SELECT l from logs l where l.user.id = :userId order by l.id desc")
                    .setParameter("userId", userId).getResultList();

            return logList;
        } catch (NoResultException e) {
            return null;
        }
    }

    public Log getLog(Long id) {

        try {
            Log log = (Log) em
                    .createQuery("SELECT l from logs l where l.id = :id")
                    .setParameter("id", id).getSingleResult();

            return log;
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean insertOrUpdateLog(Log log) {
        try {
            em.getTransaction().begin();
            if (!em.contains(log)) {
                em.merge(log);
                em.flush();
            }
            em.getTransaction().commit();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
