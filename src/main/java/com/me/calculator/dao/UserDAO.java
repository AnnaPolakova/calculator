package com.me.calculator.dao;

import com.me.calculator.model.User;
import com.me.calculator.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

public class UserDAO {

    private EntityManager em = HibernateUtils.getEm();

    public User getUser(String userName) {

        try {
            User user = (User) em
                    .createQuery("SELECT u from users u where u.userName = :name")
                    .setParameter("name", userName).getSingleResult();

            return user;
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean insertOrUpdateUser(User user) {
        try {
            em.getTransaction().begin();
            if (!em.contains(user)) {
                em.persist(user);
                em.flush();
            }
            em.getTransaction().commit();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}