package com.me.calculator.misc;

public enum OperatorEnum {
    PLUS("+"),
    MINUS("-"),
    MULTIPLICATION("*"),
    DIVISION("/"),
    DOT("."),
    EQUALS("=");

    private String sign;

    OperatorEnum(String sign) {
        this.sign = sign;
    }

    public String sign() {
        return sign;
    }
}
