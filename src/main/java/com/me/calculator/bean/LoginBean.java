package com.me.calculator.bean;

import com.me.calculator.dao.UserDAO;
import com.me.calculator.model.User;
import com.me.calculator.utils.PasswordUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.Date;

@ManagedBean
@SessionScoped
public class LoginBean {

    private boolean loggedIn;

    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;

    private UserDAO userDAO = new UserDAO();
    private User user = new User();

    public String doLogin(String username, String password) {
        user = userDAO.getUser(username);

        // successful login
        try {
            if (user != null && PasswordUtils.check(password, user.getPassword())) {
                loggedIn = true;
                user.setLastAccess(new Date());
                userDAO.insertOrUpdateUser(user);

                return navigationBean.redirectToCalculator();
            }
        } catch (Exception e) {
            e.printStackTrace();
            user = null;
        }

        // login failed
        FacesMessage msg = new FacesMessage("Login error!", "Username or password is incorrect!");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return navigationBean.toLogin();
    }

    public String doLogout() {
        loggedIn = false;

        return navigationBean.toLogin();
    }

    /*===  GETTERS & SETTERS  ===*/

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}