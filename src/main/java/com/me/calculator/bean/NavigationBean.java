package com.me.calculator.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class NavigationBean {

    // login
    public String redirectToLogin() {
        return "/ui/public/login.xhtml?faces-redirect=true";
    }

    public String toLogin() {
        return "/ui/public/login.xhtml";
    }

    // signup
    public String redirectToSignUp() {
        return "/ui/public/signup.xhtml?faces-redirect=true";
    }

    public String toSignUp() {
        return "/ui/public/signup.xhtml";
    }

    // calculator
    public String redirectToCalculator() {
        return "/ui/secured/calculator.xhtml?faces-redirect=true";
    }

    public String toCalculator() {
        return "/ui/secured/calculator.xhtml";
    }

    // log list
    public String redirectToLogList() {
        return "/ui/secured/logList.xhtml?faces-redirect=true";
    }

    public String toLogList() {
        return "/ui/secured/logList.xhtml";
    }

}