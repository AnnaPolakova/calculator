package com.me.calculator.view;

import com.me.calculator.bean.LoginBean;
import com.me.calculator.bean.NavigationBean;
import com.me.calculator.dao.LogDAO;
import com.me.calculator.misc.OperatorEnum;
import com.me.calculator.model.Log;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@ManagedBean
@ViewScoped
public class CalculatorLogView {

    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    private String logs = "";
    private String name = "";

    private Log log;
    private LogDAO logDAO = new LogDAO();

    public void logCalculation(String exp, BigDecimal result) {
        StringBuilder sb = new StringBuilder(logs);
        sb.append(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()));
        sb.append(" ==> ");
        sb.append(exp).append(OperatorEnum.EQUALS.sign()).append(result).append("\n");

        logs = sb.toString();
    }

    public String createNewLog() {
        Log log = new Log();
        log.setLogs(logs);
        log.setName(name);
        log.setDateCreated(new Date());
        log.setUser(loginBean.getUser());

        boolean ok = logDAO.insertOrUpdateLog(log);
        if (!ok) {
            FacesMessage msg = new FacesMessage("Log save error!", "Something went wrong!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);

            return navigationBean.toCalculator();
        }

        FacesMessage msg = new FacesMessage("Log is successfully saved!", "You can load it in 'My logs'!");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return navigationBean.redirectToCalculator();
    }

    /*===  GETTERS & SETTERS  ===*/

    public String getLogs() {
        return logs;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
