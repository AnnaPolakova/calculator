package com.me.calculator.view;

import com.me.calculator.misc.OperatorEnum;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.math.BigDecimal;
import java.math.RoundingMode;

@ManagedBean
@ViewScoped
public class CalculatorView {

    private String exp = "";

    private String operand = "";
    private String operator = null;
    private BigDecimal result = new BigDecimal(0);

    @ManagedProperty(value = "#{calculatorLogView}")
    private CalculatorLogView calculatorLogView;

    public void insertAction(String action) {
        if ((operator == null || operator.isEmpty()) && (operand == null || operand.isEmpty())) {
            exp = "";
            result = new BigDecimal(0);
        }

        if (!action.equals(OperatorEnum.DOT.sign()) || !operand.contains(OperatorEnum.DOT.sign())) {
            operand += action;
            exp += action;
        }
    }

    public void commandAction(String command) {
        if (exp.isEmpty()) {
            if (command.equals(OperatorEnum.MINUS.sign())) {
                operator = OperatorEnum.MINUS.sign();
                exp += OperatorEnum.MINUS.sign();
            }
        } else {
            if (operator == null || operator.isEmpty()) {
                operator = OperatorEnum.EQUALS.sign();
            }

            if (operand != null && !operand.isEmpty()) {
                calculate();
            }

            if (!exp.equals(OperatorEnum.MINUS.sign()) && !command.equals(OperatorEnum.EQUALS.sign())) {
                operator = command;
                exp = "" + result + operator;
            }
        }
    }

    private void calculate() {
        BigDecimal x = new BigDecimal(operand);

        if (operator.equals(OperatorEnum.PLUS.sign())) {
            result = result.add(x);
        } else if (operator.equals(OperatorEnum.MINUS.sign())) {
            result = result.subtract(x);
        } else if (operator.equals(OperatorEnum.MULTIPLICATION.sign())) {
            result = result.multiply(x);
        } else if (operator.equals(OperatorEnum.DIVISION.sign())) {
            try {
                result = result.divide(x);
            } catch (ArithmeticException e) {
                e.printStackTrace();
                result = result.divide(x, 15, BigDecimal.ROUND_HALF_UP);
            }
        } else if (operator.equals(OperatorEnum.EQUALS.sign())) {
            result = x;
        }

        calculatorLogView.logCalculation(exp, result);

        exp = "" + result;
        operator = null;
        operand = "";
    }

    public void clearAll() {
        exp = "";
        operator = null;
        operand = "";
        result = new BigDecimal(0);
    }

    /*===  GETTERS & SETTERS  ===*/

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public void setCalculatorLogView(CalculatorLogView calculatorLogView) {
        this.calculatorLogView = calculatorLogView;
    }
}
