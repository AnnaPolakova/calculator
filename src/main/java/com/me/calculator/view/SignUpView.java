package com.me.calculator.view;

import com.me.calculator.bean.NavigationBean;
import com.me.calculator.dao.UserDAO;
import com.me.calculator.model.User;
import com.me.calculator.utils.PasswordUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class SignUpView {

    private String userName;
    private String password;

    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;

    private UserDAO userDAO = new UserDAO();
    private User user = new User();

    public String getUserName() {
        return userName;
    }

    public String createNewUser() {
        boolean ok;

        try {
            user.setUserName(userName);
            user.setPassword(PasswordUtils.getSaltedHash(password));

            ok = userDAO.insertOrUpdateUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            ok = false;
        }

        // user save failed
        if (!ok) {
            FacesMessage msg = new FacesMessage("User save error!", "Something went wrong!");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);

            return navigationBean.toSignUp();
        }

        FacesMessage msg = new FacesMessage("User is successfully saved!", "Log in now!");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return navigationBean.redirectToLogin();
    }

    /*===  GETTERS & SETTERS  ===*/

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }
}
