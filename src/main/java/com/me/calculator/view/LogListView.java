package com.me.calculator.view;

import com.me.calculator.bean.LoginBean;
import com.me.calculator.dao.LogDAO;
import com.me.calculator.model.Log;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class LogListView {

    private List<Log> logList;
    private Log selectedLog;

    private LogDAO logDAO = new LogDAO();

    @ManagedProperty(value="#{loginBean}")
    private LoginBean loginBean;

    @PostConstruct
    public void init() {
        logList = logDAO.getLogsByUserId(loginBean.getUser().getId());
    }

    /*===  GETTERS & SETTERS  ===*/

    public List<Log> getLogList() {
        return logList;
    }

    public void setLogList(List<Log> logList) {
        this.logList = logList;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Log getSelectedLog() {
        return selectedLog;
    }

    public void setSelectedLog(Log selectedLog) {
        this.selectedLog = selectedLog;
    }
}
